from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
import textwrap


DB_DSN="sqlite:///./db"
engine = create_engine(DB_DSN)



Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(String, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return "<User(id='%s' name='%s')>" % (self.id, self.name)


class File(Base):
    __tablename__ = 'files'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    date = Column(String)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    by = relationship("User", back_populates="files")

    def __repr__(self):
        return "<File(name='%s' date='%s', by='%s')>" % (self.name, self.date, self.by.name)
User.files = relationship("File", order_by=File.date, back_populates="by")


class Post(Base):
    __tablename__ = 'posts'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    by = relationship("User", back_populates="posts")
    date = Column(String)
    text = Column(String)
    parent_post = Column(Integer, ForeignKey('posts.id'), nullable=True)  # If not null, is a comment

    def __repr__(self):
        return "<File(by='%s', date='%s'; `%s`)>" % (self.by.name, self.date, textwrap.shorten(self.text, width=10, placeholder="..."))
User.posts = relationship("Post", order_by=Post.date, back_populates="by")




Base.metadata.create_all(engine)

SQLSession = sessionmaker(bind=engine)
