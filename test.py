from IPython import embed as embedIPython
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import selenium.common.exceptions
import urllib.parse as urlparse
from urllib.parse import parse_qs
import re
import requests

from DBMap import *
from config import *


BASE_SITE = "https://www.facebook.com"


driver = None


# util to copy-pasta class attr
def cssClassSelct(class_attr):
    return "." + ".".join(class_attr.split(" "))

def getUserIdFromLink(link):
    return re.search(r"https://www.facebook.com/groups/[^/]+/user/([^/]+)/", link).group(1)

def getUserByIdOrCreateIt(id, name, sessionSql):
    if sessionSql.query(User).filter(User.id == id).count() > 0:
        return sessionSql.query(User).filter(User.id == id).one()
    userObj = User(id=id, name=name)
    sessionSql.add(userObj)
    return userObj

def wget(url, output):
    with open(output, 'wb') as fd:
        r = requests.get(url, stream=True)
        for chunk in r.iter_content(chunk_size=128):
            fd.write(chunk)

def unmaskLink(url):
    if not url.startswith('https://l.facebook.com/l.php?'):
        return url
    parsed = urlparse.urlparse(url)
    return parse_qs(parsed.query)['u'][0]

def multi_try(nb_try, exceptionType, fnc):
    ex = None
    for i in range(nb_try):
        try:
            fnc()
            return
        except exceptionType as e:
            ex = e
    if ex is not None:
        raise ex


def openGroup():
    URL_GROUP = BASE_SITE+PATH_GROUP
    global driver
    if driver is not None:
        return
    driver = webdriver.Chrome()
    driver.get(URL_GROUP)
    print("Tap [return] when you will be authenticated")
    input()
    #driver.get(URL_GROUP)
    try:
        element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, cssClassSelct("oajrlxb2 b3i9ofy5 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 j83agx80 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x cxgpxx05 d1544ag0 sj5x9vvc tw6a2znq i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l bp9cbjyn orhb3f3m czkt41v7 fmqxjp7s emzo65vh btwxx1t3 buofh1pr idiwt2bm jifvfom9 ni8dbmo4 stjgntxs kbf60n1y")))
        )
    except selenium.common.exceptions.TimeoutException:
        pass




def getFiles():
    session = SQLSession()
    FILE__LOCAL_DIRECTORY = "files"
    FILE__ROW_CSS_SELECTOR = ".scb9dxdr.sj5x9vvc.dflh9lhu.cxgpxx05.j83agx80.bp9cbjyn"
    FILE__CELL_METADATA = ".rzc85ebv.oi9244e8.bein3f3c.kb5gq1qc.mge2obcj.j83agx80.bp9cbjyn"
    FILE__CELL_METADATA_DATE = ".d2edcug0.hpfvmrgz.qv66sw1b.c1et5uql.gk29lw5a.a8c37x1j.keod5gw0.nxhoafnm.aigsh9s9.tia6h79c.fe6kdd0r.mau55g9w.c8b282yb.iv3no6db.e9vueds3.j5wam9gi.knj5qynh.oo9gr5id.hzawbc8m"
    FILE__CELL_METADATA_BY = cssClassSelct("oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8 b1v8xokw")
    FILE__CELL_MORE_BTN = '[aria-label="File options"]'
    FILE__CELL_DOWNLOAD_LNK = cssClassSelct("oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 j83agx80 p7hjln8o kvgmc6g5 oi9244e8 oygrvhab h676nmdw pybr56ya dflh9lhu f10w8fjw scb9dxdr i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l bp9cbjyn dwo3fsh8 btwxx1t3 pfnyh3mw du4w35lb")

    PATH_FILES = PATH_GROUP + "files/"
    print("Click on Files")
    driver.find_element(by=By.CSS_SELECTOR, value=('a[href="{}"]'.format(PATH_FILES))).click()
    try:
        element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, FILE__ROW_CSS_SELECTOR))
        )
    except selenium.common.exceptions.TimeoutException:
        pass
    import time
    time.sleep(1)
    print('has files')

    # While loop to load dynamicaly added elements
    old_count = 1 # to skip the header
    while True:
        fileElems = driver.find_elements(by=By.CSS_SELECTOR, value=FILE__ROW_CSS_SELECTOR)
        if old_count >= len(fileElems):
            break
        i = old_count
        old_count = len(fileElems)

        for fileElem in fileElems[i:]:
            print("Identified a new file!")
            driver.execute_script("arguments[0].scrollIntoView();", fileElem)  # Scroll to load the rest
            driver.execute_script("window.scroll(window.scrollX, window.scrollY - 130);", fileElem)  # Scroll to load the rest
            try:
                filename = fileElem.find_elements(by=By.CSS_SELECTOR, value="*")[0].get_attribute('innerText')
                metadataElem = fileElem.find_element(by=By.CSS_SELECTOR, value=FILE__CELL_METADATA)
                date = metadataElem.find_element(by=By.CSS_SELECTOR, value=FILE__CELL_METADATA_DATE).get_attribute('innerText')
                byElem = metadataElem.find_element(by=By.CSS_SELECTOR, value=FILE__CELL_METADATA_BY)
                by_name = byElem.get_attribute('innerText')
                by_id = getUserIdFromLink(byElem.get_attribute("href"))
                multi_try(10, selenium.common.exceptions.ElementClickInterceptedException, lambda: fileElem.find_element(by=By.CSS_SELECTOR, value=FILE__CELL_MORE_BTN).click())

                try:
                    element = WebDriverWait(driver, 10).until(
                        EC.visibility_of_element_located((By.CSS_SELECTOR, FILE__CELL_DOWNLOAD_LNK))
                    )
                except selenium.common.exceptions.TimeoutException:
                    pass
                downloadlink = unmaskLink(driver.find_element(by=By.CSS_SELECTOR, value=FILE__CELL_DOWNLOAD_LNK).get_attribute('href'))

                # Hide the dropdown
                import time
                time.sleep(1)
                fileElem.click()
                try:
                    element = WebDriverWait(driver, 10).until_not(
                        EC.visibility_of_element_located((By.CSS_SELECTOR, FILE__CELL_DOWNLOAD_LNK))
                    )
                except selenium.common.exceptions.TimeoutException:
                    pass

                print(filename, date, by_name, by_id, downloadlink)
            except selenium.common.exceptions.NoSuchElementException as e:
                print("Has an error")
                print(e.with_traceback())
                embedIPython()

            byUser = getUserByIdOrCreateIt(id=by_id, name=by_name, sessionSql=session)
            print(byUser)
            fileObj = File(name=filename, date=date, by=byUser)
            print(fileObj)
            session.add(fileObj)

            output = FILE__LOCAL_DIRECTORY + "/" + filename
            wget(downloadlink, output)
            print("downloaded @", output)

            session.commit()
            print("=" * 30)
    print("Finish to download files")



def getMembers():
    session = SQLSession()
    MEMBER__COUNTER = cssClassSelct('d2edcug0 hpfvmrgz qv66sw1b c1et5uql gk29lw5a jq4qci2q a3bd9o3v knj5qynh m9osqain')
    MEMBER__ROW = '.b20td4e0.muag1w35 .ue3kfks5.pw54ja7n.uo3d90p7.l82x9zwi.a8c37x1j'
    MEMBER__NAME = cssClassSelct('oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl oo9gr5id gpro0wi8 lrazzd5p')

    PATH_MEMBERS = PATH_GROUP + "members/"
    print("Click on Members")
    driver.find_element(by=By.CSS_SELECTOR, value=('a[href="{}"]'.format(PATH_MEMBERS))).click()
    try:
        element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, MEMBER__COUNTER))
        )
    except selenium.common.exceptions.TimeoutException:
        pass
    import time
    time.sleep(1)
    nb_members = int(driver.find_element(by=By.CSS_SELECTOR, value=MEMBER__COUNTER).get_attribute('innerText').split(' ')[1].replace(',', ''))
    print('has %d members' % nb_members)

    # While loop to load dynamicaly added elements
    old_count = 0
    while True:
        import time
        time.sleep(3)
        memElems = driver.find_elements(by=By.CSS_SELECTOR, value=MEMBER__ROW)
        if old_count >= len(memElems):
            break
        i = old_count
        old_count = len(memElems)

        for memElem in memElems[i:]:
            print("Identified a new member!")
            driver.execute_script("arguments[0].scrollIntoView();", memElem)  # Scroll to load the rest
            #driver.execute_script("window.scroll(window.scrollX, window.scrollY - 130);", memElem)  # Scroll to load the rest
            try:
                userElem = memElem.find_element(by=By.CSS_SELECTOR, value=MEMBER__NAME)
                user_name = userElem.get_attribute('innerText')
                user_id = getUserIdFromLink(userElem.get_attribute("href"))
                print(user_name, user_id)
            except selenium.common.exceptions.NoSuchElementException as e:
                print("Has an error")
                print(e.with_traceback())
                embedIPython()

            user = getUserByIdOrCreateIt(id=user_id, name=user_name, sessionSql=session)
            print(user)
            session.add(user)
            session.commit()
            print("=" * 30)
    print("Finish to list members. Now, I got %d members, expected %d" % (session.query(User).count(), nb_members))



openGroup()
embedIPython() # this call anywhere in your program will start IPython

#getFiles()
#embedIPython() # this call anywhere in your program will start IPython

#getMembers()
#embedIPython() # this call anywhere in your program will start IPython

getPosts()
embedIPython() # this call anywhere in your program will start IPython

#getComments()
#embedIPython() # this call anywhere in your program will start IPython

driver.close()
